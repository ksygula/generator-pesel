<?php


namespace App\Tests\service;


use App\Entity\PeselGeneratorPayload;
use App\Tools\PeselDate;
use PHPUnit\Framework\TestCase;

class PeselDateTest extends TestCase
{
    /**
     * @var PeselDate
     */
    private $peselDate;

    protected function setUp(): void
    {
        $this->peselDate = new PeselDate();
    }


    public function testPrepareDateWithRandomDate()
    {
        $generatedPeselDate = $this->peselDate->prepareDate(new PeselGeneratorPayload());
        $this->assertIsString($generatedPeselDate);
        $this->assertNotNull($generatedPeselDate);
        $this->assertStringMatchesFormat('%i', $generatedPeselDate);
        $this->assertEquals(6, strlen($generatedPeselDate));
    }

    /**
     * @param $payload
     * @param $expected
     * @param $description
     * @dataProvider dataForTestPrepareDate
     */
    public function testPrepareDate($payload, $expected, $description)
    {
        $this->assertEquals(
            $expected,
            $this->peselDate->prepareDate($payload),
            $description
        );
    }

    public function dataForTestPrepareDate(): array
    {
        return [
            [
                $this->preparePayload('1988-01-10'),
                '880110',
                'Date without month change, month with leading zero'
            ],
            [
                $this->preparePayload('1999-12-31'),
                '991231',
                'Date without month change, cut-off date'
            ],
            [
                $this->preparePayload('1888-01-10'),
                '888110',
                'Date in 1800-1899 range, 80 added to month'
            ],
            [
                $this->preparePayload('1899-12-31'),
                '999231',
                'Date in 1800-1899 range, 80 added to month, cut-off date'
            ],
            [
                $this->preparePayload('2000-01-01'),
                '002101',
                'Date in 2000-2099 range, 20 added to month, cut-off date'
            ],
            [
                $this->preparePayload('2099-12-31'),
                '993231',
                'Date in 2000-2099 range, 20 added to month, cut-off date'
            ],
            [
                $this->preparePayload('2157-07-24'),
                '574724',
                'Date in 2100-2199 range, 40 added to month'
            ],
            [
                $this->preparePayload('2157-11-24'),
                '575124',
                'Date in 2100-2199 range, 40 added to month'
            ],
            [
                $this->preparePayload('2207-12-24'),
                '077224',
                'Date in 2200-2299 range, 60 added to month'
            ]
        ];
    }

    private function preparePayload(string $date): PeselGeneratorPayload
    {
        $payload = new PeselGeneratorPayload();
        $payload->setDateOfBirth(new \DateTime($date));
        return $payload;
    }
}
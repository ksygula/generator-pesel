<?php


namespace App\Tests\service;


use App\Tools\PeselCheckDigit;
use PHPUnit\Framework\TestCase;

class PeselCheckDigitTest extends TestCase
{
    /**
     * @param string $input
     * @param int $expected
     * @dataProvider dataForTestPrepareCheckDigit
     */
    public function testPrepareCheckDigit(string $input, int $expected)
    {
        $peselCheckDigit = new PeselCheckDigit();

        $this->assertEquals($expected, $peselCheckDigit->prepareCheckDigit($input));
    }

    public function dataForTestPrepareCheckDigit(): array
    {
        return [
            ['5105256967', 0],
            ['0829161613', 1],
            ['8801100303', 2],
            ['5102015616', 3],
            ['5104131283', 4],
            ['7801187133', 5],
            ['3208165618', 6],
            ['9507287751', 7],
            ['0207080362', 8],
            ['8405197087', 9],

        ];
    }
}
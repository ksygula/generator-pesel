<?php


namespace App\Tests\service;


use App\Entity\GenderType;
use App\Entity\PeselGeneratorPayload;
use App\Service\PeselGeneratorService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use function PHPUnit\Framework\assertEquals;

class PeselGeneratorServiceTest extends KernelTestCase
{
    public function testPreparePayload()
    {
        self::bootKernel();
        $container = self::$container;
        $peselGeneratorService = $container->get(PeselGeneratorService::class);
        $payload = $peselGeneratorService->preparePayload();

        $this->assertInstanceOf(PeselGeneratorPayload::class, $payload);
        $this->assertEquals(GenderType::ANY, $payload->getGender());
    }

    /**
     * @param PeselGeneratorPayload $payload
     * @dataProvider dataForTestGenerate
     */
    public function testGenerate(PeselGeneratorPayload $payload)
    {
        self::bootKernel();
        $container = self::$container;
        $peselGeneratorService = $container->get(PeselGeneratorService::class);

        $pesel = $peselGeneratorService->generate($payload);

        $this->assertIsString($pesel);
        $this->assertStringMatchesFormat('%i', $pesel);
        $this->assertEquals(11, strlen($pesel));
    }

    public function dataForTestGenerate(): array
    {
        return [
            [$this->preparePayload(['gender' => 0, 'date_of_birth' => null])],
            [$this->preparePayload(['gender' => 0, 'date_of_birth' => new \DateTime('1988-01-10')])],
            [$this->preparePayload(['gender' => 1, 'date_of_birth' => null])],
            [$this->preparePayload(['gender' => 1, 'date_of_birth' => new \DateTime('1988-01-10')])],
            [$this->preparePayload(['gender' => 2, 'date_of_birth' => null])],
            [$this->preparePayload(['gender' => 2, 'date_of_birth' => new \DateTime('1988-01-10')])],
        ];
    }

    private function preparePayload(array $data): PeselGeneratorPayload
    {
        $payload = new PeselGeneratorPayload();
        $payload->setGender($data['gender']);
        $payload->setDateOfBirth($data['date_of_birth']);

        return $payload;
    }
}
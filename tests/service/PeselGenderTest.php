<?php


namespace App\Tests\service;


use App\Entity\GenderType;
use App\Entity\PeselGeneratorPayload;
use App\Tools\PeselGender;
use App\Tools\RandomDigitGenerator;
use PHPUnit\Framework\TestCase;

class PeselGenderTest extends TestCase
{
    public function testPrepareGenderPartAny()
    {
        $payload = new PeselGeneratorPayload();
        $payload->setGender(GenderType::ANY);

        $randomNumberMock = $this->getMockBuilder(RandomDigitGenerator::class)->getMock();
        $randomNumberMock->method('generateThreeRandomDigits')->willReturn('111');
        $randomNumberMock->method('generateRandomDigitInRange')->willReturn(1);

        $peselGender = new PeselGender($randomNumberMock);

        $generatedGenderPart = $peselGender->prepareGenderPart($payload);

        $this->assertEquals('1111', $generatedGenderPart);
    }

    /**
     * @param int $arrayKey
     * @param int $arrayValue
     * @dataProvider dataForTestPrepareGenderPartFemale
     */
    public function testPrepareGenderPartFemale(int $arrayKey, int $arrayValue)
    {
        $payload = new PeselGeneratorPayload();
        $payload->setGender(GenderType::FEMALE);

        $randomNumberMock = $this->getMockBuilder(RandomDigitGenerator::class)->getMock();
        $randomNumberMock->method('generateThreeRandomDigits')->willReturn('111');
        $randomNumberMock->method('generateRandomDigitInRange')->willReturn($arrayKey);

        $peselGender = new PeselGender($randomNumberMock);
        $generatedGenderPart = $peselGender->prepareGenderPart($payload);
        $this->assertIsString($generatedGenderPart);
        $this->assertEquals(4, strlen($generatedGenderPart));
        $this->assertEquals($arrayValue, intval(substr($generatedGenderPart, -1)));
    }

    public function dataForTestPrepareGenderPartFemale() : array
    {
        return [
          [0, 0],
          [1, 2],
          [2, 4],
          [3, 6],
          [4, 8],
        ];
    }

    /**
     * @param int $arrayKey
     * @param int $arrayValue
     * @dataProvider dataForTestPrepareGenderPartMale
     */
    public function testPrepareGenderPartMale(int $arrayKey, int $arrayValue)
    {
        $payload = new PeselGeneratorPayload();
        $payload->setGender(GenderType::MALE);

        $randomNumberMock = $this->getMockBuilder(RandomDigitGenerator::class)->getMock();
        $randomNumberMock->method('generateThreeRandomDigits')->willReturn('111');
        $randomNumberMock->method('generateRandomDigitInRange')->willReturn($arrayKey);

        $peselGender = new PeselGender($randomNumberMock);
        $generatedGenderPart = $peselGender->prepareGenderPart($payload);
        $this->assertIsString($generatedGenderPart);
        $this->assertEquals(4, strlen($generatedGenderPart));
        $this->assertEquals($arrayValue, intval(substr($generatedGenderPart, -1)));
    }

    public function dataForTestPrepareGenderPartMale() : array
    {
        return [
            [0, 1],
            [1, 3],
            [2, 5],
            [3, 7],
            [4, 9],
        ];
    }
}
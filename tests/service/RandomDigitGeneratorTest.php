<?php


namespace App\Tests\service;


use App\Tools\RandomDigitGenerator;
use PHPUnit\Framework\TestCase;

class RandomDigitGeneratorTest extends TestCase
{
    public function testGenerateThreeRandomDigits()
    {
        $generatedString = (new RandomDigitGenerator())->generateThreeRandomDigits();
        $this->assertIsString($generatedString);
        $this->assertStringMatchesFormat('%i', $generatedString);
        $this->assertEquals(3, strlen($generatedString));
    }

    public function testGenerateRandomDigitInRange()
    {
        $generatedNumber = (new RandomDigitGenerator())->generateRandomDigitInRange(0,9);
        $this->assertIsInt($generatedNumber);
        $this->assertTrue(in_array($generatedNumber, range(0,9)));
    }
}
<?php


namespace App\Tools;


use App\Entity\GenderType;
use App\Entity\PeselGeneratorPayload;

class PeselGender
{
    /**
     * @var RandomDigitGenerator
     */
    private $randomDigitGenerator;

    /**
     * PeselGender constructor.
     * @param RandomDigitGenerator $randomDigitGenerator
     */
    public function __construct(RandomDigitGenerator $randomDigitGenerator)
    {
        $this->randomDigitGenerator = $randomDigitGenerator;
    }


    public function prepareGenderPart(PeselGeneratorPayload $payload) : string
    {
        $genderPart = '';
        $genderPart .= $this->prepareSerialNumber();
        $genderPart .= $this->prepareGenderNumber($payload);

        return $genderPart;
    }

    private function prepareSerialNumber() : string
    {
        return $this->randomDigitGenerator->generateThreeRandomDigits();
    }

    private function prepareGenderNumber(PeselGeneratorPayload $payload) : int
    {
        $gender = $payload->getGender();
        if ($gender != GenderType::ANY) {
            $range = $this->getArrayOfGenderValues($gender);
            return $range[$this->getRandomValueFromArray($range)];
        }
        return $this->randomDigitGenerator->generateRandomDigitInRange(0, 9);
    }

    private function getArrayOfGenderValues(int $gender) : array
    {
        return $gender == GenderType::FEMALE ? range(0, 8, 2) : range(1,9,2);
    }

    private function getRandomValueFromArray(array $array) : int
    {
        return $this->randomDigitGenerator->generateRandomDigitInRange(
            array_key_first($array),
            array_key_last($array)
        );
    }
}
<?php


namespace App\Tools;


class PeselCheckDigit
{
    /**
     * @var array
     */
    private $weights = array(1, 3, 7, 9, 1, 3, 7, 9, 1, 3);

    public function prepareCheckDigit(string $pesel) : int
    {
        $checkSum = 0;
        foreach ($this->weights as $key => $weight) {
            $checkSum += ($this->getValueFromString($pesel, $key) * $weight) % 10;
        }

        return (10 - ($checkSum % 10)) % 10;
    }

    private function getValueFromString(string $string, int $offset) : int
    {
        return intval(substr($string, $offset, 1));
    }
}
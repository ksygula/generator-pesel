<?php


namespace App\Tools;


use App\Entity\PeselGeneratorPayload;

class PeselDate
{
    /**
     * @var array
     */
    private $monthAddedValue = array(
        array(
            'min' => '1800-01-01',
            'max' => '1899-12-31',
            'added_value' => 80
        ),
        array(
            'min' => '2000-01-01',
            'max' => '2099-12-31',
            'added_value' => 20
        ),
        array(
            'min' => '2100-01-01',
            'max' => '2199-12-31',
            'added_value' => 40
        ),
        array(
            'min' => '2200-01-01',
            'max' => '2299-12-31',
            'added_value' => 60
        ),
    );

    public function prepareDate(PeselGeneratorPayload $payload) : string
    {
        $date = $this->getDateOfBirth($payload);
        $datePart = '';
        $datePart .= $this->getYear($date);
        $datePart .= $this->getMonth($date);
        $datePart .= $this->getDay($date);

        return $datePart;
    }

    private function getDateOfBirth(PeselGeneratorPayload  $payload) : \DateTime
    {
        if (empty($payload->getDateOfBirth())) {
            return $this->prepareRandomDate();
        }
        return $payload->getDateOfBirth();
    }

    private function prepareRandomDate() : \DateTime
    {
        $min = strtotime('1800-01-01');
        $max = strtotime('2299-12-31');

        $randomTimestamp = rand($min, $max);
        return new \DateTime(date('Y-m-d', $randomTimestamp));
    }

    private function getYear(\DateTime $date) : string
    {
        return $date->format('y');
    }

    private function getMonth(\DateTime $date) : string
    {
        $month = $date->format('m');
        foreach ($this->monthAddedValue as $range) {
            $minDate = new \DateTime($range['min']);
            $maxDate = new \DateTime($range['max']);

            if ($date >= $minDate && $date <= $maxDate) {
                return intval($month) + $range['added_value'];
            }
        }
        return $month;
    }

    private function getDay(\DateTime $date) : string
    {
        return $date->format('d');
    }
}
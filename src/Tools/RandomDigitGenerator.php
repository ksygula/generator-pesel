<?php


namespace App\Tools;


class RandomDigitGenerator
{
    public function generateRandomDigitInRange(int $min, int $max) : int
    {
        return mt_rand($min,$max);
    }

    public function generateThreeRandomDigits() : string
    {
        return mt_rand(0,9).mt_rand(0,9).mt_rand(0,9);
    }
}
<?php

namespace App\Form;

use App\Entity\GenderType;
use App\Entity\PeselGeneratorPayload;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PeselGeneratorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('gender', ChoiceType::class, [
                'label_attr' => array('class' => 'radio-inline'),
                'choices' =>array(
                    'Dowolna' => GenderType::ANY,
                    'Kobieta' => GenderType::FEMALE,
                    'Mężczyzna' => GenderType::MALE
                ),
                'expanded' => true,
                'required' => true,
                'label' => 'Płeć',
                'attr' =>array('class' => 'form-control')
            ])
            ->add('dateOfBirth', DateType::class, [
                'label' => 'Data urodzenia',
                'widget' => 'single_text',
                'required' => false,
                'attr' =>array(
                    'class' => 'form-control',
                    'min' => '1800-01-01',
                    'max' => '2299-12-31'
                ),
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PeselGeneratorPayload::class,
        ]);
    }
}

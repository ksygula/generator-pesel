<?php


namespace App\Controller;


use App\Form\PeselGeneratorType;
use App\Service\PeselGeneratorService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class GeneratorController extends AbstractController
{
    /**
     * @Route("/", name="main_page", methods={"GET", "POST"})
     */
    public function index(Request $request, PeselGeneratorService $peselGeneratorService)
    {
        $form = $this->createForm(PeselGeneratorType::class, $peselGeneratorService->preparePayload());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $payload = $form->getData();
            $pesel = $peselGeneratorService->generate($payload);

            return $this->render('generator/index.html.twig', [
                'form' => $form->createView(),
                'pesel' => $pesel
            ]);
        }
        return $this->render('generator/index.html.twig', [
            'form' => $form->createView(),
            'pesel' => null
        ]);
    }
}
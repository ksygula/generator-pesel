<?php

namespace App\Entity;



class PeselGeneratorPayload
{

    /**
     * @var int
     */
    private $gender;

    /**
     * @var \DateTime
     */
    private $dateOfBirth;


    /**
     * @return int
     */
    public function getGender(): int
    {
        return $this->gender;
    }

    /**
     * @param int $gender
     */
    public function setGender(int $gender): void
    {
        $this->gender = $gender;
    }

    /**
     * @return \DateTime
     */
    public function getDateOfBirth(): ?\DateTime
    {
        return $this->dateOfBirth;
    }

    /**
     * @param \DateTime|null $dateOfBirth
     */
    public function setDateOfBirth(\DateTime $dateOfBirth = null): void
    {
        $this->dateOfBirth = $dateOfBirth;
    }
}

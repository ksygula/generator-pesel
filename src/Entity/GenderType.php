<?php


namespace App\Entity;


class GenderType
{
    const ANY = 0;
    const FEMALE = 1;
    const MALE = 2;
}
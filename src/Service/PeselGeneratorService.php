<?php


namespace App\Service;


use App\Entity\GenderType;
use App\Entity\PeselGeneratorPayload;
use App\Tools\PeselCheckDigit;
use App\Tools\PeselDate;
use App\Tools\PeselGender;

class PeselGeneratorService
{
    /**
     * @var PeselDate
     */
    private $peselDate;

    /**
     * @var PeselGender
     */
    private $peselGender;

    /**
     * @var PeselCheckDigit
     */
    private $peselCheckDigit;

    /**
     * PeselGeneratorService constructor.
     * @param PeselDate $peselDate
     * @param PeselGender $peselGender
     * @param PeselCheckDigit $peselCheckDigit
     */
    public function __construct(PeselDate $peselDate, PeselGender $peselGender, PeselCheckDigit $peselCheckDigit)
    {
        $this->peselDate = $peselDate;
        $this->peselGender = $peselGender;
        $this->peselCheckDigit = $peselCheckDigit;
    }


    public function preparePayload() : PeselGeneratorPayload
    {
        $payload = new PeselGeneratorPayload();
        $payload->setGender(GenderType::ANY);

        return $payload;
    }

    public function generate(PeselGeneratorPayload $payload) : string
    {
        $pesel = '';
        $pesel .= $this->prepareDatePart($payload);
        $pesel .= $this->prepareGenderPart($payload);
        $pesel .= $this->prepareCheckDigit($pesel);

        return $pesel;
    }

    private function prepareDatePart(PeselGeneratorPayload $payload) : string
    {
        return $this->peselDate->prepareDate($payload);
    }

    private function prepareGenderPart(PeselGeneratorPayload $payload) : string
    {
        return $this->peselGender->prepareGenderPart($payload);
    }

    private function prepareCheckDigit(string $pesel)
    {
        return $this->peselCheckDigit->prepareCheckDigit($pesel);
    }
}